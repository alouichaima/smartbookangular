import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from '../model/utilisateur';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  utilisateur = new Utilisateur();
  erreur=0;



  constructor(private AuthService : AuthService,
              private router: Router) { }

  ngOnInit(): void {

  }

  onLoggedin()
  {
    this.AuthService.getUtilisateurFromDB(this.utilisateur.email!,this.utilisateur.password!).subscribe((utilisateur:Utilisateur) => {
      if (utilisateur.password==this.utilisateur.password!)
      {
      this.AuthService.signIn(utilisateur);
      this.router.navigate(['/home']);
      }
      else
      this.erreur = 1;
    },(err) => console.log(err));
    }
  }


