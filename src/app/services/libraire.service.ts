import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {libraire} from '../model/libraire';

@Injectable({
  providedIn: 'root'
})
export class LibraireService {

 urlpath: string;
  constructor(private http: HttpClient) {
    this.urlpath = 'http://localhost:8484/SmartBookStoreREST/';
  }

  getAllLibraire()
  {
    return this.http.get(this.urlpath + 'all1');
  }


   getLibraireById(cin: number): Observable<any>
  {
    return this.http.get(this.urlpath + 'cin' + cin);
  }

  supprimerLibraire(cin: number): Observable<any>
  {
    return this.http.delete(this.urlpath + 'D1' + cin, { responseType: 'text' });
  }


 /* addLibraire(l: libraire): Observable<Object>
  {
    return this.http.post(this.urlpath + 'inscrit', l);
  }*/


  updateEtudiant(l: libraire): any
  {
    return this.http.put(this.urlpath + 'update', l);
  }
}
