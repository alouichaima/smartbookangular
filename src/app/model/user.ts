export class User { 
    cin : number; 
    nom:string;
    prenom:string; 
    email: string;
    password:string;  
    login:string;
    adresse:string;
    telephone:number;
   
    
    
    constructor(cin:number,nom:string, prenom:string, email:string, password:string,login:string,adresse:string,telephone:number) 
    { 
    this.cin = cin; 
    this.email = email; 
    this.prenom = prenom;
    this.nom = nom; 
    this.password = password; 
    this.login = login ;
    this.adresse = adresse ;
    this.telephone = telephone ;
    
    } 
    
}