import { Observable } from 'rxjs';
import { LivreService } from '../services/livre.service';
import { Component, OnInit } from '@angular/core';
import{Livre} from '../model/livre';
import { Router } from '@angular/router';


@Component({
  selector: 'app-livres',
  templateUrl: './livres.component.html',
  styleUrls: ['./livres.component.scss']
})
export class LivresComponent implements OnInit {
    livres: Livre [] = new Array;
    constructor(private livreService : LivreService ,
        private router :Router) {

         }
    ngOnInit(): void {
        this.reloadData();
    }
    reloadData():void{

        this.livreService.getAllLivre().subscribe(data=>{
            this.livres=data as Livre [];
        });
    }


    deleteLivre(numSerie:any)
    {
        this.livreService.supprimerLivre(numSerie).subscribe(data=>
          {
              console.log(data);
              this.reloadData();
          },
            error=>console.log(error));
          }
    ajouter():void{
        this.router.navigate(['/addlivre']);
    }
    toUpdate(numSerie:any){
        this.router.navigate(['/update',numSerie]);
    }

  }

  /*  livres :Livre[];

  constructor(private livreService : LivreService ,
    private router :Router) {
           this.livres = LivreService.listeLivres;


*/


 /* ngOnInit(): void {}
  /*  this.livreService.listeLivre().subscribe(prods => {
        console.log(prods);
        this.livres = prods;
        });*/



/*supprimerProduit(l: Livre)
{
  console.log("suppppp suprimé");
let conf = confirm("Etes-vous sûr ?");
if (conf)
  this.livreService.supprimerProduit(l.idProduit).subscribe(() => {
  console.log("produit supprimé");
  this.SuprimerLivreDuTableau(l);
});*/

